library omni_doc_splashscreen;

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class SplashScreen extends StatefulWidget {
  final Color colorOfBackGround;
  final String urlToLottie;
  final String pageToNavigation;

  const SplashScreen({
    Key? key, 
    required this.urlToLottie, 
    required this.colorOfBackGround, 
    required this.pageToNavigation
  }) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>  with TickerProviderStateMixin{
  bool activateSplashScreen = false;
  late final AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
    Timer(
      const Duration(milliseconds: 300),
      () => setState(() => activateSplashScreen = true)
    );
  }

  Widget splashScreen() {
    if (activateSplashScreen) {
      return Scaffold(
        backgroundColor:widget.colorOfBackGround,
        body: Center(
          child: Lottie.asset(
            widget.urlToLottie,
            controller: _controller,
            frameRate: FrameRate.max,
            animate: true,
            onLoaded: (composition) {
              _controller
              ..duration = composition.duration
              ..forward().whenComplete(
                () => Navigator.popAndPushNamed(context, widget.pageToNavigation)
              );
            },
          ),
        ),
      );
    } else {
      return Scaffold(
        backgroundColor: widget.colorOfBackGround,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return splashScreen();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}
